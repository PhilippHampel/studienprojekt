package Studienprojekt.Microservice2.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/microservice2")

public class Controller {
	@GetMapping("/test")
	public String test() {
		return "Hello From Microservice2";
	}
}
