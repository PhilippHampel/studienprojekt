package Studienprojekt.ClientLookupMicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
@EnableEurekaClient
@ComponentScan({"Controller"})
@SpringBootApplication
public class ClientLookupMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientLookupMicroserviceApplication.class, args);
	}

}
