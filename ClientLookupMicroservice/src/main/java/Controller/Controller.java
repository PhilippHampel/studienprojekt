package Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

@RestController
@RequestMapping("/clientLookupMicroservice")
public class Controller {
	
	@Autowired
	private RestTemplateBuilder tmplt;
	
	@Autowired
	private EurekaClient clnt;
	
	@RequestMapping("/clientLookupForMicroservice1")
	public String clientLookupForMicroservice1() {
		RestTemplate rst = tmplt.build();
		InstanceInfo inst = clnt.getNextServerFromEureka("Microservice1", false);
		String url = inst.getHomePageUrl() + "microservice1" + "/test";
		System.out.println(url);
		ResponseEntity<String> rsp = rst.exchange(url, HttpMethod.GET, null, String.class);
		return rsp.getBody();
	}
	
	@RequestMapping("/clientLookupForMicroservice2")
	public String clientLookupForMicroservice2() {
		RestTemplate rst = tmplt.build();
		InstanceInfo inst = clnt.getNextServerFromEureka("Microservice2", false);
		String url = inst.getHomePageUrl() + "microservice2" + "/test";
		System.out.println(url);
		ResponseEntity<String> rsp = rst.exchange(url, HttpMethod.GET, null, String.class);
		return rsp.getBody();
	}

}